use chrono::Timelike;
use rocket::serde::Deserialize;
use std::fmt;
use std::process::{Command, Stdio};

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
struct TimePayload {
    sunrise: String,
    sunset: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct TimeResult {
    results: TimePayload,
    status: String,
}

// Tontitown
const LAT: &str = "36.177856";
const LNG: &str = "-94.233543";
const SCRIPT_PATH: &str = "/opt/cron";
const STRPTIME: &str = "%I:%M:%S %p";

struct ApiError {
    message: String,
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "there was an error '{}'", self.message)
    }
}

impl fmt::Debug for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ file: {}, line: {}, message: {} }}",
            file!(),
            line!(),
            self.message
        )
    }
}

#[derive(Debug)]
enum SomeError {
    PayloadError(ApiError),
    ReqwestError(reqwest::Error),
}

impl From<ApiError> for SomeError {
    fn from(err: ApiError) -> Self {
        SomeError::PayloadError(err)
    }
}

impl From<reqwest::Error> for SomeError {
    fn from(err: reqwest::Error) -> Self {
        SomeError::ReqwestError(err)
    }
}

impl fmt::Display for SomeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SomeError::PayloadError(payload_err) => write!(f, "{}", payload_err),
            SomeError::ReqwestError(req_err) => write!(f, "{}", req_err),
        }
    }
}

impl std::error::Error for SomeError {}

fn get_today_sched() -> Result<TimePayload, SomeError> {
    let url = format!(
        "https://api.sunrise-sunset.org/json?lat={lat}=&lng={lng}",
        lat = LAT,
        lng = LNG
    );
    let body: TimeResult = reqwest::blocking::get(url)?.json()?;
    if body.status != "OK" {
        Err(SomeError::PayloadError(ApiError {
            message: body.status,
        }))
    } else {
        Ok(body.results)
    }
}

fn format_time(time: chrono::NaiveTime, state: bool) -> String {
    let cmd = format!(
        "curl -X POST -d '{{\"relay_status\": {state}}}' \"localhost:8000/api/v0/status\"",
        state = state
    );
    format!(
        "{min} {hr} * * * {cmd} # inserted by merrypi",
        hr = time.hour(),
        min = time.minute(),
        cmd = cmd,
    )
}

fn parse_shed(sched: TimePayload) -> String {
    let sunrise = chrono::NaiveTime::parse_from_str(&sched.sunrise, STRPTIME);
    let sunset = chrono::NaiveTime::parse_from_str(&sched.sunset, STRPTIME);
    match (sunrise, sunset) {
        (Ok(sr), Ok(ss)) => format!(
            "{sr}\n{ss}",
            sr = format_time(sr, false),
            ss = format_time(ss, true)
        ),
        _ => format!(
            "* */1 * * * runme # error parsing API time sunrise: {sr}, sunset: {ss}",
            sr = sched.sunrise,
            ss = sched.sunset
        ),
    }
}

fn format_cron() -> String {
    let time = get_today_sched();
    let next_job = match time {
        // If there is an error, rety every hour
        Err(e) => format!(
            "* */1 * * * {script} # {err}",
            script = SCRIPT_PATH,
            err = e
        ),
        Ok(sched) => parse_shed(sched),
    };
    format!(
        "TZ=UTC\n0 12 * * * {script}\n{next_job}\n",
        script = SCRIPT_PATH,
        next_job = next_job
    )
}

fn main() {
    let echo_child = Command::new("echo")
        .arg(format_cron())
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start echo process");

    let echo_out = echo_child.stdout.expect("Failed to open echo stdout");

    let cron_child = Command::new("crontab")
        .arg("-")
        .stdin(Stdio::from(echo_out))
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start crontab process");

    cron_child
        .wait_with_output()
        .expect("Failed to wait on crontab");
}
