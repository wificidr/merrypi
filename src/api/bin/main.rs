#[macro_use]
extern crate rocket;
use rocket::http::Status;
use rocket::serde::{json::Json, Deserialize, Serialize};
use rust_gpiozero::*;

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct LEDState {
    relay_status: bool,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct JsonResponse<'r> {
    error: &'r str,
}

fn get_led_state() -> bool {
    let led = LED::new(18);
    led.is_active()
}

fn set_led_state(state: bool) {
    let mut led = LED::new(18);
    if state {
        led.on()
    } else {
        led.off()
    }
}

#[get("/status")]
fn status() -> Result<Json<LEDState>, Status> {
    Ok(Json(LEDState {
        relay_status: get_led_state(),
    }))
}

#[post("/status", data = "<state>")]
fn new<'r>(state: Json<LEDState>) -> Result<Json<LEDState>, Json<JsonResponse<'r>>> {
    set_led_state(state.relay_status);
    Ok(Json(LEDState {
        relay_status: get_led_state(),
    }))
}

#[get("/metrics")]
fn metrics() -> String {
    format!("relay_status {}\n", get_led_state())
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/api/v0/", routes![status, new, metrics])
}
