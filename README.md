# Merrypi

This is a rapidly built and poorly designed project to prevent me
from having to walk outside and uplug the Christmas lights.

## Setup

### Dev env

Use [cross](https://github.com/cross-rs/cross).

```shell
cross build --target arm-unknown-linux-gnueabihf --release
scp target/arm-unknown-linux-gnueabihf/release/merrypi 192.168.1.218:~/src/
```

### Raspberry pi

```shell
# dpkg complains about arch mismatch between arm and armhf, but the arm binary is the one we want
wget https://github.com/cloudflare/cloudflared/releases/download/2023.10.0/cloudflared-linux-arm
chmod u+x cloudflared-linux-arm
sudo mv cloudflared-linux-arm /usr/local/bin/cloudflard
```

Use `rustls` because `openssl` is not supported on RPi (reqwest dependency).

## Testing

```shell
curl -X POST -d '{"relay_status": true}' localhost:8000/api/v0/status
```

## TODO

- [X] API
- [X] Scheduler
- [ ] Website
- [ ] Metrics
  - [X] Endpoint
  - [ ] Graphana
  - [ ] Alertmanager

```python
# Tontitown
LAT = "36.177856"
LNG = "-94.233543"
url = "https://api.sunrise-sunset.org/json?lat{LAT}=&lng={LNG}".format(LAT=LAT, LNG=LNG)
```

```json
{
  "results": {
    "sunrise": "1:02:20 PM",
    "sunset": "11:05:28 PM",
    "solar_noon": "6:03:54 PM",
    "day_length": "10:03:08",
    "civil_twilight_begin": "12:35:55 PM",
    "civil_twilight_end": "11:31:53 PM",
    "nautical_twilight_begin": "12:04:25 PM",
    "nautical_twilight_end": "12:03:24 AM",
    "astronomical_twilight_begin": "11:33:37 AM",
    "astronomical_twilight_end": "12:34:11 AM"
  },
  "status": "OK"
}
```
